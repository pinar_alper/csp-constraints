/**
 * 
 */
package uk.ac.kcl.pgt.constraints.beans;

import java.util.Map;

/**
 * @author palper
 *
 */
public abstract class DistributionConstraint {

	DistributionKindEnum kind;

	String variableName;

	public DistributionConstraint(DistributionKindEnum kind, String variableName) {
		super();
		this.kind = kind;
		this.variableName = variableName;
	}


	public DistributionKindEnum getKind() {
		return kind;
	}


	public String getVariableName() {
		return variableName;
	}

	public abstract Map<Integer, Integer> getOccurencesForInstance(int numOfInstances);

}
