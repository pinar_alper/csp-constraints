/**
 * 
 */
package uk.ac.kcl.pgt.constraints.beans;

/**
 * @author palper
 *
 */
public enum DistributionKindEnum {

	UNI("uniform"), NEG_EXP("negexponential"), PIE("pie"), NORM("normal");

	private String text;

	DistributionKindEnum(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	public static DistributionKindEnum fromString(String text) {
		for (DistributionKindEnum b : DistributionKindEnum.values()) {
			if (b.text.equalsIgnoreCase(text)) {
				return b;
			}
		}
		return null;
	}
}
