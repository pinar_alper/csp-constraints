/**
 * 
 */
package uk.ac.kcl.pgt.constraints.beans;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;

/**
 * @author palper
 *
 */
public class DomainXmlConfigReader {

	private XMLConfiguration config = null;

	public DomainXmlConfigReader(File configFile) {

		super();

		try {
			config = new XMLConfiguration(configFile);
			config.setExpressionEngine(new XPathExpressionEngine());

		} catch (Exception cex) {
			// something went wrong, e.g. the file was not found
			cex.printStackTrace();

		}
	}

	public Set<Variable> getVariables() {

		java.util.List<HierarchicalConfiguration> vars = config.configurationsAt("/variables/var");

		Set<Variable> variables = new HashSet<Variable>();

		int totalVars = vars.size();

		int i = 1; // indices start from 1 in XPATH

		while (i <= totalVars) {

			Variable v = new Variable();
			v.setName((String) config.getProperty("/variables/var[" + i + "]/@name"));
			v.setType(VarDatatypeEnum.fromString((String) config.getProperty("/variables/var[" + i + "]/@typpe")));

			String domainStr = (String) config.getProperty("/variables/var[" + i + "]/@domain");
			String[] domainVals = domainStr.split(" ");

			v.setDomainAsString(domainVals);

			variables.add(v);

			i++;

		}
		return variables;

	}

	public Set<DistributionConstraint> getDistributions() {

		java.util.List<HierarchicalConfiguration> distConfigs = config.configurationsAt("/distributions/dist");

		Set<DistributionConstraint> distConstraints = new HashSet<DistributionConstraint>();

		int totalDistributions = distConfigs.size();

		int i = 1; // indices start from 1 in XPATH

		while (i <= totalDistributions) {

			DistributionKindEnum distKind = DistributionKindEnum
					.fromString((String) config.getProperty("/distributions/dist[" + i + "]/@kind"));
			String varName = (String) config.getProperty("/distributions/dist[" + i + "]/@var");

			if (distKind.equals(DistributionKindEnum.PIE)) {

				Map<String, Double> sliceMap = new HashMap<String, Double>();

				java.util.List<HierarchicalConfiguration> slices = config
						.configurationsAt("/distributions/dist[" + i + "]/slice");

				for (HierarchicalConfiguration slice : slices) {

					sliceMap.put(slice.getString("@val"), Double.valueOf(slice.getString("@percent")));
				}
				
				PieDistribution cons = new PieDistribution(varName, sliceMap);
				
				distConstraints.add(cons);

			} else if (distKind.equals(DistributionKindEnum.UNI)) {
				
				UniformDistribution cons = new UniformDistribution(varName);
				
				distConstraints.add(cons);
				
			}else if (distKind.equals(DistributionKindEnum.NEG_EXP)) {
				
				String exponentString = (String)config.getProperty("/distributions/dist[" + i + "]/@exponent");

				NegativeExponentialDistribution cons = new NegativeExponentialDistribution(varName,Double.valueOf(exponentString));
				
				distConstraints.add(cons);
			}
			// TODO Add Normal Distribution

			i++;
		}
		return distConstraints;
	}

	public int getNumInstances() {

		String instancesStr = (String) config.getProperty("/@instances");

		return new Integer(instancesStr).intValue();
	}

}