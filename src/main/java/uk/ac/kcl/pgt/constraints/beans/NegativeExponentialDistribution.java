/**
 * 
 */
package uk.ac.kcl.pgt.constraints.beans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.distribution.ZipfDistribution;

import uk.ac.kcl.pgt.constraints.register.Register;

/**
 * @author palper
 *
 */
public class NegativeExponentialDistribution extends DistributionConstraint {
	
	private double exponent;

	public NegativeExponentialDistribution(String variableName, double exponent) {
		
		super(DistributionKindEnum.NEG_EXP, variableName);
		
		this.exponent = exponent;

	}

	/* (non-Javadoc)
	 * @see uk.ac.kcl.pgt.constraints.beans.DistributionConstraint#getOccurencesForInstance(int)
	 */
	@Override
	public Map<Integer, Integer> getOccurencesForInstance(int numOfInstances) {
		
		Map<Integer, Integer> occurences = new HashMap<Integer, Integer>();

		Variable var = Register.getInstance().getVarByName(getVariableName());

		int[] distinctValues = var.getDomain();
		
		for(int i:distinctValues){
			
			occurences.put(i, 0);
		
		}
	
		ZipfDistribution dist = new ZipfDistribution(distinctValues.length, exponent);
		
		int[] sampleArray = dist.sample(numOfInstances);
		System.out.println("------------Zipf Sample---------------");
		System.out.println(Arrays.toString(sampleArray));
		System.out.println("-------------------------------------");
		
		for(int j:sampleArray){

			occurences.put(distinctValues[j-1], (occurences.get(distinctValues[j-1])+1));
			
		}

		return occurences;
	}

}
