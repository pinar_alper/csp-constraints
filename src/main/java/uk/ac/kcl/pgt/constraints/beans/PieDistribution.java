/**
 * 
 */
package uk.ac.kcl.pgt.constraints.beans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import uk.ac.kcl.pgt.constraints.register.Register;

/**
 * @author palper
 *
 */
public class PieDistribution extends DistributionConstraint {

	Map<String, Double> slices;

	public PieDistribution(String variableName, Map<String, Double> slices) {

		super(DistributionKindEnum.PIE, variableName);

		this.slices = slices;

	}

//	@Override
//	public Map<Integer, Integer> getOccurencesForInstance(int numOfInstances){
//		
//		Map<Integer, Integer> occurences = new HashMap<Integer, Integer>();
//		
//		//TODO: Do this better, to make sure we do not deviate from slice percentages. 
//		//a library that will allow me do the  sampling from a  probability distribution while checking for sum of occurences as a
//		// a constraint.
//		
//		int summOccurences = 0;
//		for(Map.Entry<String, Double> entry:slices.entrySet()){
//
//			Integer intInchoco = Register.getInstance().getStringRegister().getChocoIntegerForString(entry.getKey());
//			Integer numOccurence = Double.valueOf(entry.getValue()*numOfInstances).intValue();
//			//System.out.println("Value: "+intInchoco+"  Num of Occurences: " + numOccurence);
//			occurences.put(intInchoco, numOccurence);
//			summOccurences+=numOccurence;
//		}
//		
//		//System.out.println("Sum Occurences: " + summOccurences);
//		if (summOccurences<numOfInstances){
//			int counter = 0;
//			while (summOccurences<numOfInstances){
//				
//				Integer res = getFirstEntryKeyHavingValue(counter,occurences);
//				if (res != null){
//					occurences.put(res, counter+1);
//					summOccurences++;
//					if (!occurences.containsValue(counter)){ counter++;}
//				}else{
//					counter++;
//				}
//			}
//			
//		}
//		return occurences;
//	}

//	private Integer getFirstEntryKeyHavingValue(Integer val, Map<Integer,Integer> map){
//		for(Map.Entry<Integer,Integer> entry:map.entrySet()){
//			if (entry.getValue().equals(val))  return entry.getKey();
//		}
//
//		return null;
//	}
	
	@Override
	public Map<Integer, Integer> getOccurencesForInstance(int numOfInstances) {

		Map<Integer, Integer> occurences = new HashMap<Integer, Integer>();

		Variable var = Register.getInstance().getVarByName(getVariableName());

		int[] distinctValues = var.getDomain();
		
		for(int i:distinctValues){
			
			occurences.put(i, 0);
		
		}

		int[] res =  new int[slices.entrySet().size()];
		double[] probs = new double[slices.entrySet().size()];
	
		int indice = 0;
		
		for(Map.Entry<String, Double> entry:slices.entrySet()){
			
			Integer intInchoco = Register.getInstance().getStringRegister().getChocoIntegerForString(entry.getKey());
			res[indice] = intInchoco;
			probs[indice] = entry.getValue();
	
			indice++;
		}
		EnumeratedIntegerDistribution dist = new EnumeratedIntegerDistribution(res, probs);
		
		int[] sampleArray = dist.sample(numOfInstances);
		System.out.println("------------Pie Sample---------------");
		System.out.println(Arrays.toString(sampleArray));
		System.out.println("-------------------------------------");
		for(int j:sampleArray){

			occurences.put(j, (occurences.get(j)+1));
			
		}

		return occurences;
	}
}
