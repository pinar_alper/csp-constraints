/**
 * 
 */
package uk.ac.kcl.pgt.constraints.beans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.distribution.UniformIntegerDistribution;

import uk.ac.kcl.pgt.constraints.register.Register;

/**
 * @author palper
 *
 */
public class UniformDistribution extends DistributionConstraint {

	public UniformDistribution(String variableName) {

		super(DistributionKindEnum.UNI, variableName);

	}

//	@Override
//	public Map<Integer, Integer> getOccurencesForInstance(int numOfInstances) {
//
//		Map<Integer, Integer> occurences = new HashMap<Integer, Integer>();
//
//		Variable var = Register.getInstance().getVarByName(getVariableName());
//
//		int[] distinctValues = var.getDomain();
//
//		int summOccurences = (Double.valueOf(numOfInstances / distinctValues.length)).intValue()
//				* distinctValues.length;
//
//		int difference = numOfInstances - summOccurences;
//
//		for (int i : distinctValues) {
//
//			if (difference > 0) {
//				occurences.put(Integer.valueOf(i),
//						(Double.valueOf(numOfInstances / distinctValues.length)).intValue() + 1);
//				difference--;
//			} else if (difference == 0) {
//				occurences.put(Integer.valueOf(i), (Double.valueOf(numOfInstances / distinctValues.length)).intValue());
//			}
//
//		}
//		return occurences;
//	}
	
	@Override
	public Map<Integer, Integer> getOccurencesForInstance(int numOfInstances) {

		Map<Integer, Integer> occurences = new HashMap<Integer, Integer>();

		Variable var = Register.getInstance().getVarByName(getVariableName());

		int[] distinctValues = var.getDomain();
		
		for(int i:distinctValues){
			
			occurences.put(i, 0);
		
		}
	
		UniformIntegerDistribution dist = new UniformIntegerDistribution(1,distinctValues.length);
		
		int[] sampleArray = dist.sample(numOfInstances);
		System.out.println("------------Uniform Sample---------------");
		System.out.println(Arrays.toString(sampleArray));
		System.out.println("-------------------------------------");
		for(int j:sampleArray){

			occurences.put(distinctValues[j-1], (occurences.get(distinctValues[j-1])+1));
			
		}

		return occurences;
	}
}
