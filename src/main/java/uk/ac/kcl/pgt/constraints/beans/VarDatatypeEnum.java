/**
 * 
 */
package uk.ac.kcl.pgt.constraints.beans;

/**
 * @author palper
 *
 */
public enum VarDatatypeEnum {

	INTEGER("int"), STRING("str"), DATE("date");

    String text;

	VarDatatypeEnum(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	public static VarDatatypeEnum fromString(String text) {
		for (VarDatatypeEnum b : VarDatatypeEnum.values()) {
			if (b.text.equalsIgnoreCase(text)) {
				return b;
			}
		}
		return null;
	}

}
