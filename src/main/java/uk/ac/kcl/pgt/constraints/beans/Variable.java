/**
 * 
 */
package uk.ac.kcl.pgt.constraints.beans;

/**
 * @author palper
 *
 */
public class Variable {

	String name;
	VarDatatypeEnum type;
	int[] domain;
	String[] domainAsString;

	public String[] getDomainAsString() {
		
		return domainAsString;
		
	}

	public void setDomainAsString(String[] domainAsString) {
		
		this.domainAsString = domainAsString;
		
	}

	public int[] getDomain() {
		return domain;
	}

	public void setDomain(int[] domain) {
		this.domain = domain;
	}

	public VarDatatypeEnum getType() {
		return type;
	}

	public void setType(VarDatatypeEnum type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(Object o) {
		if (o instanceof Variable) {
			Variable c = (Variable) o;
			if (this.getName().equals(c.getName()))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (this.name == null)
			return 0;
		return this.name.hashCode();
	}
}
