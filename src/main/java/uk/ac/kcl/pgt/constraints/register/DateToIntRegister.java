/**
 * 
 */
package uk.ac.kcl.pgt.constraints.register;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;

/**
 * @author palper
 *
 */
public class DateToIntRegister {

	Map<String, Integer> map;
	Map<Integer, String> reversemap;	
	
	Integer offSet = 3000;

	public DateToIntRegister() {
		super();
		map = new HashMap<String, Integer>();
		reversemap = new HashMap<Integer, String>();
	}

	public Integer registerDateString(String dateStr){
		
		Integer daysSinceStart = daysSinceStart(dateStr);
		
		map.put(dateStr,offSet+daysSinceStart);
		reversemap.put(offSet+daysSinceStart, dateStr);
		
		return offSet+daysSinceStart;
	}
	private int daysSinceStart(String dateStr){
		
		DateTime start = new DateTime(2000, 1, 1, 1, 0);
	    		
	    DateTime date = new DateTime(Integer.parseInt(dateStr.substring(0,4)), Integer.parseInt(dateStr.substring(4,6)), Integer.parseInt(dateStr.substring(6,8)), 1, 0);
		
	    return Days.daysBetween(new LocalDate(start), new LocalDate(date)).getDays() ;
	    
	}
	
	public int mapSize(){
		return map.keySet().size();
	}
	
	public int getRegisteredDate(String dateString){
		return map.get(dateString);
	}
	public String getRegisteredDate(Integer dateAsInt){
		return reversemap.get(dateAsInt);
	}

}
