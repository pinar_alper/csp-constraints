/**
 * 
 */
package uk.ac.kcl.pgt.constraints.register;

import java.util.Set;

import uk.ac.kcl.pgt.constraints.beans.DistributionConstraint;
import uk.ac.kcl.pgt.constraints.beans.VarDatatypeEnum;
import uk.ac.kcl.pgt.constraints.beans.Variable;

/**
 * @author palper
 *
 */
public class Register {

	private DateToIntRegister dateRegister;

	private StrToIntRegister stringRegister;

	private Set<Variable> variableRegister;

	private Set<DistributionConstraint> distributionConstraints;

	private static Register instance = null;

	private static boolean initialized = false;

	public boolean isInitialized() {
		return initialized;
	}

	public static Register getInstance() {

		if (instance == null) {

			instance = new Register();
			return instance;

		} else {

			return instance;
		}
	}

	public void initialize(Set<Variable> variables, Set<DistributionConstraint> distCons) {

		// Because Choco and most other CSP solvers work with integers
		// we map string and date domains to integer values

		if (!initialized) {

			dateRegister = new DateToIntRegister();

			stringRegister = new StrToIntRegister();

			distributionConstraints = distCons;

			variableRegister = variables;

			for (Variable v : variableRegister) {

				int[] domain = new int[v.getDomainAsString().length];

				for (int j = 0; j < domain.length; j++) {

					if (v.getType().equals(VarDatatypeEnum.STRING)) {

						domain[j] = stringRegister.registerString(v.getDomainAsString()[j]);

					} else if (v.getType().equals(VarDatatypeEnum.DATE)) {

						domain[j] = dateRegister.registerDateString(v.getDomainAsString()[j]);

					}
				}
				v.setDomain(domain);

			}

			initialized = true;
		}
	}

	public Variable getVarByName(String name) {

		Variable result = null;

		for (Variable var : variableRegister) {

			if (var.getName().equals(name))

				result = var;
		}

		return result;
	}

	public DateToIntRegister getDateRegister() {

		if (isInitialized()) {

			return dateRegister;

		} else

			throw new RuntimeException();

	}

	public StrToIntRegister getStringRegister() {

		if (isInitialized()) {

			return stringRegister;

		} else

			throw new RuntimeException();

	}

	public Set<Variable> getVariableRegister() {

		if (isInitialized()) {

			return variableRegister;

		} else

			throw new RuntimeException();
	}
	

	public Set<DistributionConstraint> getDistributionConstraints() {
		return distributionConstraints;
	}

	public String getVarValueString(Variable var, Integer val) {
		
		if (var.getType().equals(VarDatatypeEnum.DATE)) {
			return getDateRegister().getRegisteredDate(val);
			
		}else if (var.getType().equals(VarDatatypeEnum.STRING)){
			return getStringRegister().getRegisteredString(val);
		}else{
			// TODO Will the register to support variables typed as integer at the Provenance Template level
			return null;
		}

	}


}
