/**
 * 
 */
package uk.ac.kcl.pgt.constraints.register;

import java.util.HashMap;
import java.util.Map;

/**
 * @author palper
 *
 */
public class StrToIntRegister {

	private Map<String, Integer> map;
	private Map<Integer, String> reversemap;	
	private Integer offSet = 1000;
	
	public StrToIntRegister() {
		super();
		map = new HashMap<String, Integer>();
		reversemap = new HashMap<Integer, String>();
	}

	public Integer registerString(String str){
		
		map.put(str,offSet);
		reversemap.put(offSet, str);
		offSet++;
		return offSet-1;
	}
	public Integer getChocoIntegerForString(String str){
		Integer result = null;
		for(Map.Entry<String, Integer> ent:map.entrySet()){
			if (ent.getKey().equals(str)){
				
				 result = ent.getValue();
			}
			
		}
		return result;
	}
	public String getRegisteredString(Integer strAsInt){
		return reversemap.get(strAsInt);
	}

}
