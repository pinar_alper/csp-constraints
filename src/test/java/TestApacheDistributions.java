import java.util.Arrays;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.ZipfDistribution;
import org.junit.Test;

/**
 * 
 */

/**
 * @author k1633135
 *
 */
public class TestApacheDistributions {

	@Test
	public void test1() {
		int counter = 5;
		while (counter > 0) {
			UniformIntegerDistribution dist = new UniformIntegerDistribution(0, 4);
			int[] res = dist.sample(10);
			int zeros = 0;
			int ones = 0;
			int twos = 0;
			int threes = 0;
			int fours = 0;
			int fives = 0;

			for (int i : res) {
				if (i == 0) {
					zeros++;
				}
				if (i == 1) {
					ones++;
				}
				if (i == 2) {
					twos++;
				}
				if (i == 3) {
					threes++;
				}
				if (i == 4) {
					fours++;
				}
				if (i == 5) {
					fives++;
				}
			}
			System.out.println(Arrays.toString(res));
			System.out.println("Zeros: " + zeros + "\t" + "Ones: " + ones + "\t" + "Twos: " + twos + "\t" + "Threes: "
					+ threes + "\t" + "Fours: " + fours + "\t" + "Fives: " + fives);

			counter--;
		}
	}

	@Test
	public void test2() {
		int[] res = { 1, 2, 3, 4 };
		double[] probs = { 0.45, 0.1, 0.4, 0.05 };
		EnumeratedIntegerDistribution dist = new EnumeratedIntegerDistribution(res, probs);

		int[] samp = dist.sample(100);

		int ones = 0;
		int twos = 0;
		int threes = 0;
		int fours = 0;

		for (int i : samp) {
			if (i == 1) {
				ones++;
			}
			if (i == 2) {
				twos++;
			}
			if (i == 3) {
				threes++;
			}
			if (i == 4) {
				fours++;
			}
		}
		System.out.println(Arrays.toString(res));
		System.out.println("Ones: " + ones + "\t" + "Twos: " + twos + "\t" + "Threes: " + threes + "\t" + "Fours: "
				+ fours + "\t");

	}

	@Test
	public void test3() {

		int[] res = { 1, 2, 3, 4 , 5};
		ZipfDistribution dist = new ZipfDistribution(res.length, 1.07);

		int[] sampleArray = dist.sample(100);
		System.out.println("------------Zipf Sample---------------");
		System.out.println(Arrays.toString(sampleArray));
		System.out.println("-------------------------------------");

	}

}
