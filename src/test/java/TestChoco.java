import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.IIntConstraintFactory;
import org.chocosolver.solver.variables.IntVar;

/**
 * 
 */

/**
 * @author palper
 *
 */
public class TestChoco {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Model model = new Model();
		IntVar visit1 = model.intVar("visit1", new int[]{1000,1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009});
		IntVar visit2 = model.intVar("visit2", new int[]{1000,1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009});
		IntVar visit3 = model.intVar("visit3", new int[]{1000,1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009});

		model.arithm(visit2, ">", model.intOffsetView(visit1, 3)).post();
		model.arithm(visit3, ">=", visit2).post();


		model.getSolver().showSolutions();
		model.getSolver().findAllSolutions();
	}

}
