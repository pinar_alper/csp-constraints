import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;

/**
 * 
 */

/**
 * @author palper
 *
 */
public class TestDistribution {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	
			Model model = new Model();
			
			BoolVar[][] participations = model.boolVarMatrix(100, 4);
		

			for (int i = 0; i < participations.length; i++) {
				model.sum(participations[i], "=",1).post();
				
			}
			
			//A1 A2 A3 A4
			//24 21 21 34
			//Total:100
			System.out.println(ArrayUtils.getColumn(participations, 0));
	
			model.sum(ArrayUtils.getColumn(participations, 0), "=", 24).post();
			model.sum(ArrayUtils.getColumn(participations, 1), "=", 21).post();
			model.sum(ArrayUtils.getColumn(participations, 2), "=", 21).post();
			model.sum(ArrayUtils.getColumn(participations, 3), "=", 34).post();
			
		
			model.getSolver().showSolutions();
			model.getSolver().findSolution();
			//we find a single solution.
		}

}
