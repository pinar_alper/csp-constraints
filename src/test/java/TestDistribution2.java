import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.util.tools.ArrayUtils;

/**
 * 
 */

/**
 * @author palper
 *
 */
public class TestDistribution2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		Model model = new Model();
		
		BoolVar[][] participations = model.boolVarMatrix(10, 4);
	

		for (int i = 0; i < participations.length; i++) {
			model.sum(participations[i], "=",2).post();
			model.ifThen(
					   model.arithm(participations[i][0],"=",1),
					   model.arithm(participations[i][3],"=",0)
					);
			
		}	
		//A1 A2 A3 A4
		//2 6   10  2 
		//Total:10
		System.out.println(ArrayUtils.getColumn(participations, 0));
		
		model.sum(ArrayUtils.getColumn(participations, 0), "=", 2).post();
		model.sum(ArrayUtils.getColumn(participations, 1), "=", 6).post();
		model.sum(ArrayUtils.getColumn(participations, 2), "=", 10).post();
		model.sum(ArrayUtils.getColumn(participations, 3), "=", 2).post();
		
	
		model.getSolver().showSolutions();
		model.getSolver().findAllSolutions();
		//we find a single solution.
	}

}
