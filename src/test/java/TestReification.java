import org.chocosolver.solver.Model;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.SetVar;

/**
 * 
 */

/**
 * @author palper
 *
 */
public class TestReification {

	public static void main(String[] args) {
		
		Model model = new Model();
		IntVar diagnosis = model.intVar("diagnosis", new int[]{1,2,3});
		SetVar treatments = model.setVar("treatments", new int[]{1}, new int[]{1,2,3,4,5});
		

		model.ifThen(
				   model.arithm(diagnosis,"=",1),
				   model.member(2,treatments)
				);

		model.ifThen(
				   model.arithm(diagnosis,"=",3),
				   model.notMember(4, treatments)
				);
		model.ifThen(
				   model.arithm(diagnosis,"=",3),
				   model.notMember(5, treatments)
				);
		

		model.getSolver().showSolutions();
		model.getSolver().findAllSolutions();
	}
	

}
