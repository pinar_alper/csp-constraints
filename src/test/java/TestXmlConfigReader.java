import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

import uk.ac.kcl.pgt.constraints.beans.CspSolution;
import uk.ac.kcl.pgt.constraints.beans.DistributionConstraint;
import uk.ac.kcl.pgt.constraints.beans.DomainXmlConfigReader;
import uk.ac.kcl.pgt.constraints.beans.Variable;
import uk.ac.kcl.pgt.constraints.choco.ChocoCSPInstance;
import uk.ac.kcl.pgt.constraints.register.DateToIntRegister;
import uk.ac.kcl.pgt.constraints.register.Register;

public class TestXmlConfigReader {

	@Test
	@Ignore
	public void testXMLReader() {

		DomainXmlConfigReader reader = new DomainXmlConfigReader(
				new File(DomainXmlConfigReader.class.getResource("Tapp2017UseCaseDomain.xml").getPath()));
		Set<Variable> vars = reader.getVariables();
		int num = reader.getNumInstances();
		Set<DistributionConstraint> dists = reader.getDistributions();

		assertEquals("numInstances read from test config file should be 10", 10, num);
		assertEquals("number of variables read from test config file should be 6", 6, vars.size());
		assertEquals("number of distribution constraints read from test config file should be 2 ", 2, dists.size());

	}

	@Test
	@Ignore
	public void testRegisterInit() {
		DomainXmlConfigReader reader = new DomainXmlConfigReader(
				new File(DomainXmlConfigReader.class.getResource("Tapp2017UseCaseDomain.xml").getPath()));
		Set<Variable> vars = reader.getVariables();
		Set<DistributionConstraint> dists = reader.getDistributions();

		Register reg = Register.getInstance();

		reg.initialize(vars, dists);

		DateToIntRegister dreg = reg.getDateRegister();
		assertEquals("number of distinct dates within domains of all date variables should be 31", 31, dreg.mapSize());
		// Check whether the register contains the necessary number of variables
		// etc

		assertEquals(dreg.getRegisteredDate("20170101"), 9210);
		assertEquals(dreg.getRegisteredDate("20170102"), 9211);

	}

	@Test
	public void testSolutionWithDistributionConstraints() {
		DomainXmlConfigReader reader = new DomainXmlConfigReader(
				new File(DomainXmlConfigReader.class.getResource("Tapp2017UseCaseDomain.xml").getPath()));
		Set<Variable> vars = reader.getVariables();
		int num = reader.getNumInstances();
		Set<DistributionConstraint> dists = reader.getDistributions();

		Register reg = Register.getInstance();

		reg.initialize(vars, dists);
		
		ChocoCSPInstance inst = new ChocoCSPInstance(num, reg);

		CspSolution[] result = inst.solve();
		
		for (CspSolution sol:result){
			
			 Map<Variable,String> map = sol.getVariableValueMap();
			 
			 for (Map.Entry<Variable, String> entry:map.entrySet()){
				 
				 System.out.println("Var " + entry.getKey().getName() +  "\t Value " + entry.getValue());
				 
			 }
		}
		
	}

}
